using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

public class BallHandler : MonoBehaviour
{
    [SerializeField] GameObject ballPrefab;
    [SerializeField] Rigidbody2D pivot;
    [SerializeField] float detachDelay = 0.5f;
    [SerializeField] float respawnDelay = 1f;

    Camera mainCamera;
    bool isDragging;
    Rigidbody2D currentBallRigidbody;
    SpringJoint2D currentBallSpringJoint;

    void Start(){
        mainCamera = Camera.main;
        SpawnBall();
    }

    void OnEnable() {
        EnhancedTouchSupport.Enable();
    }

    void OnDisable() {
        EnhancedTouchSupport.Disable();
    }

    void Update(){
        if(currentBallRigidbody == null)return;
        if(Touch.activeTouches.Count == 0){
            if(isDragging){
                LaunchBall();
            }
            isDragging=false;
            return;
        }
        isDragging=true;
        currentBallRigidbody.isKinematic = true;
        Vector2 touchPositions = new Vector2();
        foreach (Touch touch in Touch.activeTouches){
            touchPositions += touch.screenPosition;
        }
        touchPositions /= Touch.activeTouches.Count;
        
        Vector3 worldPosition = mainCamera.ScreenToWorldPoint(touchPositions);
        currentBallRigidbody.position = worldPosition;
    }

    void LaunchBall(){
        currentBallRigidbody.isKinematic = false;
        currentBallRigidbody = null;
        Invoke(nameof(DetachBall),detachDelay);
    }

    void DetachBall(){
        currentBallSpringJoint.enabled = false;
        currentBallSpringJoint = null;
        Invoke(nameof(SpawnBall),respawnDelay);
    }

    void SpawnBall(){
        GameObject ballInstance = Instantiate(ballPrefab, pivot.position,Quaternion.identity);
        currentBallRigidbody = ballInstance.GetComponent<Rigidbody2D>();
        currentBallSpringJoint = ballInstance.GetComponent<SpringJoint2D>();
        currentBallSpringJoint.connectedBody = pivot;
    }
    
    
}
